#!/bin/bash

# Verificar se foi fornecido um parâmetro
if [ $# -ne 1 ]; then
    echo "Uso: $0 <caminho_da_pasta>"
    exit 1
fi

# Atribuir o parâmetro a uma variável
pasta=$1

# Verificar se o caminho especificado é um diretório
if [ ! -d "$pasta" ]; then
    echo "Erro: O caminho fornecido não é um diretório."
    exit 1
fi

# Inicializar contadores
quantidade_arquivos=0
quantidade_diretorios=0

# Loop para contar arquivos e diretórios
for item in "$pasta"/*; do
    if [ -f "$item" ]; then
        quantidade_arquivos=$((quantidade_arquivos + 1))
    elif [ -d "$item" ]; then
        quantidade_diretorios=$((quantidade_diretorios + 1))
    fi
done

# Imprimir resultados
echo "Quantidade de arquivos em $pasta: $quantidade_arquivos"
echo "Quantidade de diretórios em $pasta: $quantidade_diretorios"
