#!/bin/bash

# Verificar se está sendo executado como usuário "ifpb"
if [ "$(whoami)" != "ifpb" ]; then
    echo "Erro: Este script deve ser executado como usuário 'ifpb'."
    exit 1
fi

# Verificar se foi fornecido um argumento de linha de comando
if [ $# -eq 0 ]; then
    # Se nenhum argumento foi fornecido, usar o caminho /home/ifpb como padrão
    pasta="/home/ifpb"
else
    # Se um argumento foi fornecido, atribuir à variável pasta
    pasta="$1"
fi

# Verificar se o caminho especificado é um diretório
if [ ! -d "$pasta" ]; then
    echo "Erro: O caminho fornecido não é um diretório."
    exit 1
fi

# Inicializar contadores
quantidade_arquivos=0
quantidade_diretorios=0

# Loop para contar arquivos e diretórios
for item in "$pasta"/*; do
    if [ -f "$item" ]; then
        quantidade_arquivos=$((quantidade_arquivos + 1))
    elif [ -d "$item" ]; then
        quantidade_diretorios=$((quantidade_diretorios + 1))
    fi
done

# Imprimir resultados
echo "Quantidade de arquivos em $pasta: $quantidade_arquivos"
echo "Quantidade de diretórios em $pasta: $quantidade_diretorios"
